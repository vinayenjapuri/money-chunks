export enum CONTENT_TYPES {
  "Home"="Home",
  "About"="About",
  "FAQ"="FAQ",
  "How to Earn"="How to Earn",
}

export enum USER_CONTENT_TYPES {
  "Dashboard" = "Dashboard",
  "AccountSettings" = "Account Settings",
}
