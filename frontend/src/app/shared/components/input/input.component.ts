import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ReactiveFormsModule,
  ValidationErrors,
  Validator, Validators
} from "@angular/forms";

@Component({
  selector: 'mc-input',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    }
  ],
  templateUrl: './input.component.html',
  styleUrl: './input.component.scss'
})
export class InputComponent implements ControlValueAccessor, Validator, OnInit {
  @Input({required: true}) label!: string;
  @Input({required: true}) placeholder!: string;
  @Input() type: string = 'text';
  @Input() hint: string | null = null;
  onChange = (value:any) => {};
  onValidate = () => {};
  onTouched?: any;
  control = new FormControl<string | null>(null, [Validators.required]);
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  validate(control: AbstractControl<any, any>): ValidationErrors | null {
    if (control?.value?.length > 0) {
      return null;
    } else {
      return  {
        invalid: true,
      };
    }
  }
  registerOnValidatorChange?(fn: () => void): void {
    this.onValidate = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.control.disable();
    } else {
      this.control.enable();
    }
  }

  writeValue(obj: any): void {
    this.control.setValue(obj);
  }

  ngOnInit(): void {
    this.control.valueChanges.subscribe((v) => {
      if (this.onChange) {
        this.onChange(v);
        this.onValidate();
      }
    });
  }

  onTextChanges() {
  }
}
