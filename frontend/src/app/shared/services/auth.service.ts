import {Injectable} from '@angular/core';
import {BehaviorSubject, from, Observable} from "rxjs";
import {GoogleAuthProvider} from "firebase/auth";
import {Auth, getAuth, signInWithPopup} from "@angular/fire/auth";
import {FirebaseApp} from "@angular/fire/app";
import {AuthenticateComponent} from "../../contents/authenticate/authenticate.component";
import {MatDialog} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {MoneyChunksState} from "../../store/mc-state";
import {setContentTypes, setCurrentUser} from "../../store/money-chunks.actions";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CONTENT_TYPES} from "../enums/content.enum";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  provider = new GoogleAuthProvider();
  auth: Auth;
  contentHeader: BehaviorSubject<any> = new BehaviorSubject<any>({
    signup: false,
    login: false
  });

  constructor(private fApp: FirebaseApp, private store: Store<MoneyChunksState>, private matDialog: MatDialog, private router: Router, private matSnackbar: MatSnackBar) {
    this.auth = getAuth(this.fApp);
  }

  login(): Observable<any> {
    let result: any;
    const log = async () => {
      return await signInWithPopup(this.auth, this.provider);
    }
    return from(log().then((u) => {
      result = u;
      if (!this.auth.currentUser?.emailVerified) {
        this.matSnackbar.open('Your email has not been verified by Google. Please contact administrator', '', {
          duration: 4000
        });
        return null;
      }
      return result?.user;
    }).catch((err) => {
      if (err?.message?.indexOf('auth/popup-blocked') > -1) {
        this.matSnackbar.open('Please disable the popup blocker.', 'Dismiss');
      } else {
        this.matSnackbar.open('Auto authentication is failed. Please try again.', '', {
          duration: 4000
        })
      }
      return null;
    }));
  }

  onLogin(redirectRoute?: string) {
    if (!redirectRoute) {
      redirectRoute = 'engagement';
    }
    const ref = this.matDialog.open(AuthenticateComponent, {
      hasBackdrop: true,
      disableClose: true,
      delayFocusTrap: true,
      id: 'mc-dialog-authentication'
    });
    this.login().subscribe((user) => {
      console.log(user);
      if (user) {
        ref.componentInstance.message = 'Authentication Successful';
        ref.componentInstance.error = false;
        ref.componentInstance.showLoader = false;
        ref.componentInstance.success = true;
        this.store.dispatch(setCurrentUser(Object.freeze(user)));
      } else {
        ref.componentInstance.message = 'Authentication Failed.';
        ref.componentInstance.error = true;
        ref.componentInstance.showLoader = false;
        this.store.dispatch(setCurrentUser(undefined));
      }
      setTimeout(() => {
        ref.close();
        user && this.router.navigate([redirectRoute], {
          replaceUrl: true,
        });
        !user && this.router.navigate(['home']);
      }, 500);
    })
  }

  logout(): void {
    this.auth.signOut().then(() => {
      this.router.navigate(['home']).then(() => {
        this.store.dispatch(setCurrentUser(undefined));
        this.store.dispatch(setContentTypes(CONTENT_TYPES.Home));
      });
    });
  }
}
