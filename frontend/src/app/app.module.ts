import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatButtonModule} from "@angular/material/button";
import {HomeComponent} from "./contents/home/home.component";
import {AboutComponent} from "./contents/about/about.component";
import {FaqComponent} from "./contents/faq/faq.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {environment} from "../environments/environment";
import {mcReducer} from "./store/mc-reducers";
import {getFirestore, provideFirestore} from "@angular/fire/firestore";
import {provideFirebaseApp, initializeApp } from "@angular/fire/app";
import {AuthenticateComponent} from "./contents/authenticate/authenticate.component";
import {provideCountdown} from "ngx-countdown";

const firebaseConfig = {
  apiKey: "AIzaSyC8HsBotsObVnO5_JebeI7uBvU07o33xcs",
  authDomain: "money-ch.firebaseapp.com",
  databaseURL: "https://money-ch-default-rtdb.firebaseio.com",
  projectId: "money-ch",
  storageBucket: "money-ch.appspot.com",
  messagingSenderId: "1040251698469",
  appId: "1:1040251698469:web:8399e114f0c20acdcfeecc"
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FlexLayoutModule,
    MatButtonModule,
    HomeComponent,
    AboutComponent,
    AuthenticateComponent,
    FaqComponent,
    StoreModule.forRoot({mcReducer}, {
      }),
    StoreModule.forFeature('money-chunksState', mcReducer),
    provideFirebaseApp(() => initializeApp(firebaseConfig)),
    provideFirestore(() => getFirestore()),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  providers: [
    provideCountdown({ format: `mm:ss` })
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
