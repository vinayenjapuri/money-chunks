import { createFeatureSelector, createSelector } from "@ngrx/store";
import {MoneyChunksState} from "./mc-state";

export const selectMCState = createFeatureSelector<MoneyChunksState>('money-chunksState');

export const getCurrentContent = createSelector(selectMCState, (state: MoneyChunksState) => state?.currentContent);
export const getCurrentUser = createSelector(selectMCState, (state: MoneyChunksState) => state?.currentUser);
