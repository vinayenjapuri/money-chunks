import {CONTENT_TYPES, USER_CONTENT_TYPES} from "../shared/enums/content.enum";
import firebase from "firebase/compat";
import User = firebase.User;

export interface MoneyChunksState {
  currentContent: CONTENT_TYPES,
  currentUserContent?: USER_CONTENT_TYPES;
  currentUser?: User
}
