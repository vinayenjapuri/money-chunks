import {createReducer, on} from '@ngrx/store';
import {MoneyChunksState} from "./mc-state";
import {CONTENT_TYPES} from "../shared/enums/content.enum";
import {setContentTypes, setCurrentUser, setUserContent} from "./money-chunks.actions";
import {getCurrentContent} from "./mc-selectors";


export const mcState: MoneyChunksState = {
  currentContent: CONTENT_TYPES.Home,
  currentUserContent: undefined,
  currentUser: undefined
}

export const mcReducer = createReducer(
  mcState,
  on(setContentTypes, (state, {currentContent}) => ({
    ...state,
    currentContent: currentContent
  })),
  on(setUserContent, (state, {currentUserContent}) => ({
    ...state,
    currentUserContent: currentUserContent
  })),
  on(setCurrentUser, (state, {currentUser}) => ({
    ...state,
    currentUser: currentUser
  })),
  /*on(setMoviesAggregate, (state, {moviesAggregate}) => ({
    ...state,
    moviesAggregate: moviesAggregate
  })),
  on(setSeriesAggregate, (state, {seriesAggregate}) => ({
    ...state,
    seriesAggregate: seriesAggregate
  })),
  on(setAnimesAggregate, (state, {animesAggregate}) => ({
    ...state,
    moviesAggregate: animesAggregate
  })),
  on(setMangasAggregate, (state, {mangasAggregate}) => ({
    ...state,
    moviesAggregate: mangasAggregate
  })),
  on(setSelectedAggregate, (state, {selectedAggregate}) => ({
    ...state,
    selectedAggregate: selectedAggregate
  })),
  on(setMobileResolution, (state, {isMobileResolution}) => ({
    ...state,
    mobileResolution: isMobileResolution
  })),
  on(setInnerWidth, (state, {innerWidth}) => ({
    ...state,
    innerWidth: innerWidth
  })),
  on(setSelectedGenreOnType, (state, {selectedGenres}) => ({
    ...state,
    selectedGenres: selectedGenres
  }))*/
)
