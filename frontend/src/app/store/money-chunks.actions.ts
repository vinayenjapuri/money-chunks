import {createAction} from "@ngrx/store";
import {CONTENT_TYPES, USER_CONTENT_TYPES} from "../shared/enums/content.enum";
import firebase from "firebase/compat";
import User = firebase.User;

export const setContentTypes = createAction('setContentTypes', (currentContent: CONTENT_TYPES) => ({currentContent}));
export const setUserContent = createAction('setUserContent', (currentUserContent: USER_CONTENT_TYPES) => ({currentUserContent}));
export const setCurrentUser = createAction('setCurrentUser', (currentUser?: User) => ({currentUser}));
