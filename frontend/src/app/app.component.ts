import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {CONTENT_TYPES} from "./shared/enums/content.enum";
import {AuthService} from "./shared/services/auth.service";
import {Store} from "@ngrx/store";
import {getCurrentContent, getCurrentUser} from "./store/mc-selectors";
import {MoneyChunksState} from "./store/mc-state";
import {setContentTypes} from "./store/money-chunks.actions";
import {MatDialog} from "@angular/material/dialog";
import {AuthenticateComponent} from "./contents/authenticate/authenticate.component";
import firebase from "firebase/compat";
import User = firebase.User;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  contents: string[] = [CONTENT_TYPES.Home, CONTENT_TYPES.About, CONTENT_TYPES["How to Earn"], CONTENT_TYPES.FAQ];
  currentContent: CONTENT_TYPES = CONTENT_TYPES.Home;
  currenUser?: User;

  constructor(private router: Router, private authService: AuthService, private store: Store<MoneyChunksState>, private matDialog: MatDialog) {
    const currentRoute = window.location.pathname;
    if (currentRoute?.indexOf('home') > -1 || currentRoute === '/') {
      this.currentContent = CONTENT_TYPES.Home;
    } else if (currentRoute?.indexOf('faq') > -1) {
      this.currentContent = CONTENT_TYPES.FAQ;
    } else if (currentRoute?.indexOf('how-to-earn') > -1) {
      this.currentContent = CONTENT_TYPES["How to Earn"];
    } else if (currentRoute?.indexOf('about') > -1) {
      this.currentContent = CONTENT_TYPES.About;
    } else {
      this.authService.onLogin(currentRoute);
    }
    this.store.dispatch(setContentTypes(this.currentContent));
  }

  ngOnInit() {
    this.subscribeToCurrentContent();
    this.subscribeToCurrentUser();
  }

  subscribeToCurrentUser() {
        this.store.select(getCurrentUser).subscribe((user) => {
          this.currenUser  = user;
        });
    }

  onNavigate(b: string) {
    //@ts-ignore
    this.currentContent = b === this.currentContent ? CONTENT_TYPES.home: CONTENT_TYPES[b];
    b = b === 'How to Earn'? 'how-to-earn': b;
    this.router.navigate([b?.toString()?.toLowerCase()]).then()
  }

  onLogin() {
    this.authService.onLogin();
  }

  private subscribeToCurrentContent() {
    this.store.select(getCurrentContent).subscribe((currentContent) => {
      console.log(currentContent);
      this.currentContent = currentContent;
    });
  }
}
