import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import {FlexLayoutModule} from "@angular/flex-layout";
import {DashboardComponent} from "./dashboard/dashboard.component";


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    FlexLayoutModule
  ]
})
export class UserModule { }
