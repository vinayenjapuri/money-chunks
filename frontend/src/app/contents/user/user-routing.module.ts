import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DashboardComponent} from "./dashboard/dashboard.component";
import {AccountDetailsComponent} from "./account-details/account-details.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'user',
    pathMatch: "full",
  },
  {
    path: 'user',
    loadComponent: () => import("./user/user.component").then((c) => c.UserComponent),
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      }
    ]
  },
  {
    path: 'account-settings',
    component: AccountDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
