import {Component, OnInit} from '@angular/core';
import {FlexModule} from "@angular/flex-layout";
import {MatButtonModule} from "@angular/material/button";
import {RouterOutlet} from "@angular/router";
import {USER_CONTENT_TYPES} from "../../../shared/enums/content.enum";
import {Store} from "@ngrx/store";
import {MoneyChunksState} from "../../../store/mc-state";
import {AuthService} from "../../../shared/services/auth.service";
import {getCurrentUser} from "../../../store/mc-selectors";
import firebase from "firebase/compat";
import User = firebase.User;
import {CommonModule} from "@angular/common";

export interface DashboardAggregate {
  name: string,
}

@Component({
  selector: 'mc-user',
  standalone: true,
    imports: [
      CommonModule,
        FlexModule,
        MatButtonModule,
        RouterOutlet
    ],
  templateUrl: './user.component.html',
  styleUrl: './user.component.scss'
})
export class UserComponent implements OnInit {
  currentUserContent: USER_CONTENT_TYPES = USER_CONTENT_TYPES.Dashboard;
  userContents: string[] = ['Dashboard', 'Account Settings'];
  currentUser?: User;
  dashboardContents: string[] = ['Faucet']

  constructor(private store: Store<MoneyChunksState>, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.subscribeToCurrentUser();
  }

  subscribeToCurrentUser() {
    this.store.select(getCurrentUser).subscribe((user) => {
      this.currentUser = user;
    });
  }
  onUserNavigate(b: string) {

  }

  onLogout() {
    this.authService.logout();
  }
}
