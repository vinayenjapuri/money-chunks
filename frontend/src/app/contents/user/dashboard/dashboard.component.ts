import {Component, OnInit, ViewChild} from '@angular/core';
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatButtonModule} from "@angular/material/button";
import {RouterOutlet} from "@angular/router";
import {CommonModule} from "@angular/common";
import {MatCardModule} from "@angular/material/card";
import {CountdownComponent, CountdownEvent} from "ngx-countdown";

@Component({
  selector: 'mc-dashboard',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    RouterOutlet,
    FlexLayoutModule,
    MatCardModule,
    CountdownComponent
  ],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss'
})
export class DashboardComponent {

  //Faucet initial context
  faucetDisabled: boolean = true;
  @ViewChild('faucetCountdown') faucetCountdown!: CountdownComponent;
  onFaucetClick(): void {
    this.faucetDisabled = false;
    setTimeout(() => {
      this.faucetDisabled = true;
      this.faucetCountdown.begin();
    }, 10000);
  }

  handleEvent($event: CountdownEvent) {
    this.faucetDisabled = $event.action != 'done';
  }
}
