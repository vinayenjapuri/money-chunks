import {Component, Input, OnInit} from '@angular/core';
import {FlexModule} from "@angular/flex-layout";
import {MatCardModule} from "@angular/material/card";
import {InputComponent} from "../../shared/components/input/input.component";
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import {MatButtonModule} from "@angular/material/button";
import {MatDividerModule} from "@angular/material/divider";
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatIconModule} from "@angular/material/icon";
import {AuthService} from "../../shared/services/auth.service";

@Component({
  selector: 'mc-user-authentication',
  standalone: true,
  imports: [
    FlexModule,
    MatCardModule,
    InputComponent,
    MatButtonModule,
    MatDividerModule,
    ReactiveFormsModule,
    MatInputModule,
    MatProgressBarModule,
    MatIconModule,
    RouterLink
  ],
  templateUrl: './user-authentication.component.html',
  styleUrl: './user-authentication.component.scss'
})
export class UserAuthenticationComponent implements OnInit {
  @Input() login: boolean = false;
  @Input() signUp: boolean = false;
  userControl = new FormControl('');
  passwordControl = new FormControl('');
  public loginErrorMessage!: string;
  public signupFormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.max(7)]),
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    confirmPassword: new FormControl('', [Validators.required]),
  });
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private authService: AuthService) {
  }

  ngOnInit() {
    this.subscribeToContentHeader();
  }

  authenticateCreds() {
    if (this.userControl.invalid || this.passwordControl.invalid) {
      this.loginErrorMessage = 'Username/Password is incorrect';
    } else {
      this.loginErrorMessage = '';
    }
  }

  onNavigate(nav: string) {
    if (!this.signUp || nav !=='signup') {
      nav==='home' && this.authService.contentHeader.next({
        signup: false,
        login: false
      });
      this.router.navigate([nav]).then();
    } else {
      if (this.signupFormGroup.invalid) {
        this.loginErrorMessage = 'Please enter values to proceed.'
      }
      console.log(this.signupFormGroup.value)
    }
  }

  private subscribeToContentHeader() {
    this.authService.contentHeader.subscribe((s: any) => {
      this.login = s.login;
      this.signUp = s.signup;
    })
  }
}
