import { Component } from '@angular/core';
import {MatDialogContent} from "@angular/material/dialog";
import {FlexModule} from "@angular/flex-layout";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatIconModule} from "@angular/material/icon";

@Component({
  selector: 'mc-authenticate',
  standalone: true,
  imports: [
    MatDialogContent,
    FlexModule,
    MatProgressSpinnerModule,
    MatIconModule
  ],
  templateUrl: './authenticate.component.html',
  styleUrl: './authenticate.component.scss'
})
export class AuthenticateComponent {
  message = 'Authenticating By Google';
  showLoader: boolean = true;
  error: boolean = false;
  success: boolean = false;
}
